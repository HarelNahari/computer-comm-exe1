#include <stdio.h>
#include <errno.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include "mtwister.h"
#pragma comment(lib, "Ws2_32.lib")

#define FILE_BLOCK_SIZE 7
#define SOCKET_BLOCK_SIZE 8
#define BLOCKS_PER_PACKET 8
#define MAX_ERR_LEN 50


void print_win_api_error(DWORD error_code)
{
	char *error_msg;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&error_msg,
		0, NULL);
	fprintf(stderr, "Error: %s\n", error_msg);
	LocalFree(error_msg);
}

void print_crt_error(int error_code)
{
	char error_msg[MAX_ERR_LEN] = { 0 };
	strerror_s(error_msg, MAX_ERR_LEN, error_code);
	fprintf(stderr, "Error: %s\n", error_msg);
}

/* check whether the received parameters are valid ip, ports, seed and probabilty. return -1 upon error */
int check_arg_validity(int argc, char *argv[])
{
	unsigned int i;
	int port;
	int ip[4];
	/*check for enough arguements*/
	if (argc != 6)
	{
		fprintf(stderr, "Incorrect number of arguements: %i expected 5 \n"\
			"parameters: <channel port> <receiver ip> <receiver port> <probabily x 2E16> <seed>", argc - 1);
		return -1;
	}
	/* check that inputted ip is valid */
	if (sscanf_s(argv[2], "%d.%d.%d.%d", &ip[0], &ip[1], &ip[2], &ip[3]) == 'EOF')
	{
		fprintf(stderr, "Error: ip must be of the format num.num.num.num !\n");
		return -1;
	}
	for (i = 0; i < 4; i++)
	{
		if ((ip[i] < 0) || (ip[i] > 255))
		{
			fprintf(stderr, "Error: ip must be four numbers in the range 0-255");
			return -1;
		}
	}

	/* check that inputted ports are vaild (numbers & in range) */
	/* first ip */
	for (i = 0; i < strlen(argv[1]); i++)
	{
		if (!isdigit(argv[1][i]))
		{
			fprintf(stderr, "Error: port must be an integr number!\n");
			return -1;
		}
	}
	port = atoi(argv[1]);
	if ((port < 0) || (port > 65535))
	{
		fprintf(stderr, "Error: port number must be 0-65535!\n");
		return -1;
	}
	/* seconf ip */
	for (i = 0; i < strlen(argv[3]); i++)
	{
		if (!isdigit(argv[3][i]))
		{
			fprintf(stderr, "Error: port must be an integr number!\n");
			return -1;
		}
	}
	port = atoi(argv[3]);
	if ((port < 0) || (port > 65535))
	{
		fprintf(stderr, "Error: port number must be 0-65535!\n");
		return -1;
	}
	/* check that probabilty and seed are numbers */
	for (i = 0; i < strlen(argv[4]); i++)
	{
		if (!isdigit(argv[4][i]))
		{
			fprintf(stderr, "Error: probabily must be an integr number!\n");
			return -1;
		}
	}
	for (i = 0; i < strlen(argv[5]); i++)
	{
		if (!isdigit(argv[1][i]))
		{
			fprintf(stderr, "Error: seed must be an integr number!\n");
			return -1;
		}
	}
	return 0;
}

int init_WinSock()
{
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR)
	{
		fputs("Error at WSAStartup()\n", stderr);
		return 1;
	}
	return 0;
}

/* initialize socket address structure. return 0 when successful and -1 upon error */
int init_sockaddr_in(struct sockaddr_in *addr, char *ip, int port)
{
	if (!addr)
	{
		fprintf(stderr, "Error: coudln't initialzie. NULL address pointer.\n");
		return -1;
	}
	addr->sin_family = AF_INET; /* internetwork protocol family*/
	InetPton(AF_INET, ip, &(addr->sin_addr)); /* loopback IP */
	addr->sin_port = htons(port); /* PORT */
	return 0;
}

/* flip bit in given location in buffer */
int flip_bit(char buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET], int location)
{
	if (!buffer)
	{
		fprintf(stderr, "Error: couldn't flip bit. buffer is NULL\n");
		return -1;
	}
	if ((location >= (SOCKET_BLOCK_SIZE * BLOCKS_PER_PACKET * 8)) || (location < 0))
	{
		fprintf(stderr, "Error: couldn't flip bit. location is out of buffer range.\n");
		return -1;
	}
	int byte_location = location / 8;
	unsigned int bit_location = location % 8;
	/* flip the bit in appropriate location */
	buffer[byte_location] = buffer[byte_location] ^ (1 << (7 - bit_location));
	return 0;
}

/* efficient function for power with integr exponent */
float power(float base, int exp)
{
	float result = 1;
	for (;;)
	{
		if (exp & 1)
			result *= base;
		exp >>= 1;
		if (!exp)
			break;
		base *= base;
	}
	return result;
}

/* flip bits according to probabilty and seed. return the number of bytes flipped */
int add_noise(char buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET], int prob, MTRand *r)
{
	/* calculate probability of success in a trial to flip the bits */
	float p = (float)prob / (((unsigned int)1) << 16);
	/* the number of trials is the number of bits in a packet */
	int n = SOCKET_BLOCK_SIZE * BLOCKS_PER_PACKET * 8;
	int k;
	/* intermediate value use to convert uniform to binomial distribution */
	double temp = 0;
	int flipped = 0;

	/* the number of bytes to be flipped is binomially distributred with probability p
	in order to get binomial from undiform distribution we must inverse according to the 
	cumalative distribution function of binomial RV */
	double prob_lookup_table[SOCKET_BLOCK_SIZE * BLOCKS_PER_PACKET * 8 + 1] = { 0 };
	double CDF[SOCKET_BLOCK_SIZE * BLOCKS_PER_PACKET * 8 + 1] = { 0 };
	prob_lookup_table[0] = power(1 - p, n);
	CDF[0] = prob_lookup_table[0];
	for (k = 1; k <= n; k++)
	{
		/* the mathemathical relation between consecutive binomial probabilites */
		prob_lookup_table[k] = prob_lookup_table[k - 1] * (p / (1 - p))*(n - k + 1) / k;
		/* property of CDF */
		CDF[k] = CDF[k - 1] + prob_lookup_table[k];
	}
	
	
	/* get random number from uniform distribution */
	temp = genRand(r);
	/* convert uniform to binomial distribution */
	if (temp <= CDF[0])
	{
		flipped = 0;
		return flipped;
	}
	for (k = 1; k <= n; k++)
	{
		if ((temp > CDF[k - 1]) && (temp <= CDF[k]))
		{
			flipped = k;
			break;
		}
	}
	/* after calculating the number of bits we want to flip we need to randomly pick
	the location of the bits we need to flip. we must pay attention not to flip bits more than once */
	int is_flipped[SOCKET_BLOCK_SIZE * BLOCKS_PER_PACKET * 8] = { 0 };
	int location = 0;
	for (k = 0; k < flipped;)
	{
		/* random number between 0 and 511 */
		location = (int)(genRand(r) * 511);
		if (!is_flipped[location])
		{
			if (flip_bit(buffer, location) == -1)
				return -1;
			is_flipped[location] = 1;
			k++;
		}
	}
	return flipped;
}

int same_address(struct sockaddr_in addr1, struct sockaddr_in addr2)
{
	return ((addr1.sin_addr.s_addr == addr1.sin_addr.s_addr) && (addr1.sin_port == addr2.sin_port));
}

int channel_activity(SOCKET channel_sock, struct sockaddr_in *channel_addr, \
	struct sockaddr_in *receiver_addr, struct sockaddr_in *sender_addr, int prob, MTRand *r)
{
	int status = 0;
	int chunk_recvd = 0;
	int recvd = 0;
	int sent = 0;
	int flipped = 0;
	char buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET] = { 0 };
	struct sockaddr_in temp_addr;
	socklen_t addr_len = sizeof(temp_addr);

	while (TRUE)
	{
		/* recv data directed at the channel and check for errors.
			if data is valid idetify who the data came from - sender or receiver
			and react accordingly */
		chunk_recvd = recvfrom(channel_sock, buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, 0, \
			(SOCKADDR*)&temp_addr, &addr_len);
		if (chunk_recvd == SOCKET_ERROR)
		{
			print_win_api_error(WSAGetLastError());
			return -1;
		}
		/* if data came from the receiver and we have the sender address than read second
		feedback frame and send feedback to sender */
		else if (sender_addr && same_address(temp_addr, *receiver_addr))
		{
			status = sendto(channel_sock, buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, 0, \
				(SOCKADDR*)sender_addr, sizeof(*sender_addr));
			if (status == SOCKET_ERROR)
			{
				print_win_api_error(WSAGetLastError());
				return -1;
			}
			break;
		}
		/* if data came from sender add errors according to probability and pass to receiver */
		else if (chunk_recvd)
		{
			if (!sender_addr)
			{
				sender_addr = malloc(sizeof(temp_addr));
				sender_addr->sin_family = AF_INET;
				sender_addr->sin_addr.s_addr = temp_addr.sin_addr.s_addr;
				sender_addr->sin_port = temp_addr.sin_port;
			}
			status = add_noise(buffer, prob, r);
			if (status == -1)
				return -1;
			flipped += status;
			status = sendto(channel_sock, buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, 0, \
				(SOCKADDR*)receiver_addr, sizeof(*receiver_addr));
			if (status == SOCKET_ERROR)
			{
				print_win_api_error(WSAGetLastError());
				return -1;
			}
			sent += status;
		}
		recvd += chunk_recvd;
	}
	char sender_ip[INET_ADDRSTRLEN];
	char receiver_ip[INET_ADDRSTRLEN];
	InetNtop(AF_INET, &(sender_addr->sin_addr), sender_ip, INET_ADDRSTRLEN);
	InetNtop(AF_INET, &(receiver_addr->sin_addr), receiver_ip, INET_ADDRSTRLEN);
	fprintf(stderr, "sender: %s\nreceiver: %s\n%i bytes, flipped %i bits\n", sender_ip, receiver_ip, sent, flipped);
	free(sender_addr);
	return 0;
}

int main(int argc, char* argv[])
{
	if (check_arg_validity(argc, argv) == -1)
		return -1;
	int my_port = atoi(argv[1]);
	char *receiver_ip = argv[2];
	int receiver_port = atoi(argv[3]);
	int prob = atoi(argv[4]);
	int seed = atoi(argv[5]);
	int recvd = 0;
	int sent = 0;
	int status = 0;

	/* initialize random number generator with seed */
	MTRand r = seedRand(seed);
	/* initialization of socket and address */
	init_WinSock();
	SOCKET channel_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	struct sockaddr_in* sender_addr = NULL;
	struct sockaddr_in receiver_addr;
	struct sockaddr_in channel_addr;
	/* convert INADDR_ANY to string represntaion for init_sockaddr_in() */
	int x = INADDR_ANY;
	char my_ip[20];
	inet_ntop(AF_INET, &x, my_ip, 20);

	/*initialize socket address and check for errors*/
	if ((init_sockaddr_in(&receiver_addr, receiver_ip, receiver_port) == -1) || \
		(init_sockaddr_in(&channel_addr, my_ip, my_port) == -1))
	{
		/* attempt closing the socket */
		if (closesocket(channel_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* bind the socket to IP and PORT */
	if (bind(channel_sock, (SOCKADDR*)&channel_addr, sizeof(channel_addr)) == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		if (closesocket(channel_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* finally do channel stuff */
	if (channel_activity(channel_sock, &channel_addr, &receiver_addr, sender_addr, prob, &r) == -1)
	{
		if (closesocket(channel_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	if (closesocket(channel_sock) == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	WSACleanup();
	system("pause");
	return 0;
}