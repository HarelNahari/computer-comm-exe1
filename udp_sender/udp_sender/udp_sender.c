#include <stdio.h>
#include <errno.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")

#define FILE_BLOCK_SIZE 7
#define SOCKET_BLOCK_SIZE 8
#define BLOCKS_PER_PACKET 8
#define MY_PORT 64003
#define MAX_ERR_LEN 50

static unsigned int read_leftover_bits = 0;
static unsigned char read_leftover_char = 0;

void print_win_api_error(DWORD error_code)
{
	char *error_msg;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&error_msg,
		0, NULL);
	fprintf(stderr, "Error #%i: %s\n", error_code, error_msg);
	LocalFree(error_msg);
}

void print_crt_error(int error_code)
{
	char error_msg[MAX_ERR_LEN] = { 0 };
	strerror_s(error_msg, MAX_ERR_LEN, error_code);
	fprintf(stderr, "Error: %s\n", error_msg);
}

/* check whether the received parameters are valid ip, port and file name. return -1 upon error */
int check_arg_validity(int argc, char *argv[])
{
	unsigned int i;
	int port;
	int ip[4];
	/*check for enough arguements*/
	if (argc != 4)
	{
		fprintf(stderr, "Incorrect number of arguements: %i expected 3 \n"\
			"parameters: <ip> <port> <file name>\n", argc - 1);
		return -1;
	}
	/* check that inputted ip is valid */
	if (sscanf_s(argv[1], "%d.%d.%d.%d", &ip[0], &ip[1], &ip[2], &ip[3]) == 'EOF')
	{
		fprintf(stderr, "Error: ip must be of the format num.num.num.num !\n");
		return -1;
	}
	for (i = 0; i < 4; i++)
	{
		if ((ip[i] < 0) || (ip[i] > 255))
		{
			fprintf(stderr, "Error: ip must be four numbers in the range 0-255");
			return -1;
		}
	}
	/* check that inputted port is a number */
	for (i = 0; i < strlen(argv[2]); i++)
	{
		if (!isdigit(argv[2][i]))
		{
			fprintf(stderr, "Error: port must be a number!\n");
			return -1;
		}
	}
	port = atoi(argv[2]);
	if ((port < 0) || (port > 65535))
	{
		fprintf(stderr, "Error: port number must be 0-65535!\n");
		return -1;
	}
	/* file path is checked when we try opening the file, no need to check here */
	return 0;
}

int init_WinSock()
{
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR)
	{
		fputs("Error at WSAStartup()\n", stderr);
		return 1;
	}
	return 0;
}

/* rearrange the 49MSB bits in src into a 7x7 bit map in dst return 0 upon success and -1 upon error */
int read_rearrange(unsigned char src_buffer[SOCKET_BLOCK_SIZE - 1], unsigned char dst_buffer[SOCKET_BLOCK_SIZE])
{
	if ((!src_buffer) || (!dst_buffer))
	{
		fprintf(stderr, "Error: Couldn't rearrange NULL buffers");
		return -1;
	}
	int i = 0;
	/* take the bits read so far and rearrange them so we will have 7x7 bit matrix */
	for (i = 0; i < SOCKET_BLOCK_SIZE - 2; i++)
	{
		/* The bit in the i'th place is made up from the last read_leftover_bits from the
		i'th cell in temp_buff and (8 - read_leftover_bits) from the next cell in temp_buff.
		This results in a 8 bits from which we take the last bit and store it in the 7th
		cell of buffer, in the receiveing end we shall decode it according to this convention */
		dst_buffer[i] = (src_buffer[i] << (8 - read_leftover_bits)) | (src_buffer[i + 1] >> read_leftover_bits);
		dst_buffer[SOCKET_BLOCK_SIZE - 2] = dst_buffer[SOCKET_BLOCK_SIZE - 2] | (dst_buffer[i] & 1);
		dst_buffer[SOCKET_BLOCK_SIZE - 2] = dst_buffer[SOCKET_BLOCK_SIZE - 2] << 1;
		/* after the 8th bit from the i'th cell is stored in the 7th cell
		zero it in the i'th cell for parity check later on*/
		dst_buffer[i] = dst_buffer[i] & ~1;
	}
	/* we put the last bits from the first six bytes in the 7th cell. now we add to
	that the last bit, (which is the #leftover bit from the end) to complete the 49 bits */
	dst_buffer[SOCKET_BLOCK_SIZE - 2] = dst_buffer[SOCKET_BLOCK_SIZE - 2] | \
		((src_buffer[SOCKET_BLOCK_SIZE - 2] >> (read_leftover_bits - 1)) & 1);
	dst_buffer[SOCKET_BLOCK_SIZE - 2] = dst_buffer[SOCKET_BLOCK_SIZE - 2] << 1;
	return 0;
}

/* get 8x8 bits sized char buffer populated only in the inner 7x7 bits and add
	parity bits in outer row and collumn bits */
int add_parity(unsigned char buffer[SOCKET_BLOCK_SIZE])
{
	if (!buffer)
	{
		fprintf(stderr, "Error: couldn't add parity, buffer is NULL.\n");
		return -1;
	}

	unsigned int i, j;
	int count;
	for (i = 0; i < SOCKET_BLOCK_SIZE; i++)
	{
		count = 0;
		/* count the number of 1s in the i'th row to see if it has odd/even parity */
		for (j = 0; j < 8; j++)
		{
			if (buffer[i] & (1 << j))
				count++;
		}
		/* if parity is odd flip the 8th bit to get even parity */
		if (count % 2)
			buffer[i] = buffer[i] ^ 1;
		count = 0;
		/* count the number of 1s in the i'th column to see if it has odd/even parity */
		for (j = 0; j < 8; j++)
		{
			if (buffer[j] & (1 << (7 - i)))
				count++;
		}
		/* if parity is odd flip the i'th bit in the last cell to get even parity */
		if (count % 2)
			buffer[SOCKET_BLOCK_SIZE - 1] = buffer[SOCKET_BLOCK_SIZE - 1] ^ (1 << (7 - i));
	}
	return 0;
}

/* read 49 bits from file, store leftover in char and keep track of position with another variable
	return the actual number of bits read from file and -1 upon error */
int read_n_encode(FILE *fp, unsigned char buffer[SOCKET_BLOCK_SIZE])
{
	int bits_read = 0;
	int i;
	unsigned char read_buffer[SOCKET_BLOCK_SIZE - 1] = { 0 };
	if (!buffer)
	{
		fprintf(stderr, "Error: couldn't add parity, buffer is NULL.\n");
		return -1;
	}
	for (i = 0; i < SOCKET_BLOCK_SIZE; i++)
		buffer[i] = 0;

	/* if there are no bits leftover read 8 bits into leftover and treat them as leftover */
	if (!read_leftover_bits)
	{
		read_leftover_bits = 8;
		bits_read = fread_s(&read_leftover_char, 1, 1, 1, fp) * 8;
		if (!bits_read)
			return 0;
	}

	/* put the leftover in the first place and read all the new bytes into the temp buffer */
	read_buffer[0] = read_leftover_char;
	bits_read += fread_s(read_buffer + 1, SOCKET_BLOCK_SIZE - 1, FILE_BLOCK_SIZE - 1, 1, fp) * (FILE_BLOCK_SIZE - 1) * 8;
	if (ferror(fp))
	{
		print_crt_error(errno);
		return -1;
	}
	/* the last char in temp_buff is the one from which there will be leftovers */
	read_leftover_char = read_buffer[SOCKET_BLOCK_SIZE - 2];

	/* take the bits read so far and rearrange them so we will have 7x7 bit matrix */
	if (read_rearrange(read_buffer, buffer) == -1)
		return -1;
	/* add the correct parity bit for each 8 bit cell in the array */
	if (add_parity(buffer) == -1)
		return -1;
	/* the number of leftover bits is now decreased by 1 since we read 48 bits and sent 49 */
	read_leftover_bits--;
	return bits_read;
}

/* initialize socket address structure. return 0 when successful and -1 upon error */
int init_sockaddr_in(struct sockaddr_in *addr, char *ip, int port)
{
	if (!addr)
	{
		fprintf(stderr, "Error: coudln't initialzie. NULL address pointer.\n");
		return -1;
	}
	addr->sin_family = AF_INET; /* internetwork protocol family*/
	InetPton(AF_INET, ip, &(addr->sin_addr)); /* loopback IP */
	addr->sin_port = htons(port); /* PORT */
	return 0;
}

/* send file to channel. return number of bytes sent or -1 upon error */
int send_file(SOCKET s, char *file_name, struct sockaddr_in addr)
{
	int i;
	int read = 0;
	int chunk_read = 0;
	int sent = 0;
	int status = 0;
	char read_buffer[FILE_BLOCK_SIZE] = { 0 };
	char send_buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET] = { 0 };
	FILE *fp;
	/* open file for reading and handle errors if any */
	fopen_s(&fp, file_name, "rb");
	if (!fp)
	{
		print_crt_error(errno);
		return -1;
	}

	/* keep reading until reaching EOF */
	while (!feof(fp))
	{
		chunk_read = 0;
		/* read bytes from file. read BLOCKS_PER_PACKET*SOCKET_BLOCK_SIZE bytes and then send them as 1 group */
		for (i = 0; i < BLOCKS_PER_PACKET; i++)
		{
			status = read_n_encode(fp, send_buffer + SOCKET_BLOCK_SIZE * i);
			if (status == -1)
				return -1;
			chunk_read += status;
		}
		if (chunk_read)
		{
			read += FILE_BLOCK_SIZE;
			status = sendto(s, send_buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, 0, \
				(SOCKADDR*)&addr, sizeof(addr));
			if (status == -1)
			{
				print_win_api_error(WSAGetLastError());
				return -1;
			}
			sent += status;
		}
	}
	fclose(fp);
	return sent;
}

/* recv feedback from the receiver (through the channel).
 return number of bytes received or -1 upon error.  */
int recv_feedback(SOCKET s)
{
	int chunk_recvd = 0;
	char recv_buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET] = { 0 };
	int feedback[4];

	chunk_recvd = recvfrom(s, recv_buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, 0, NULL, NULL);
	if (chunk_recvd == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		return -1;
	}
	sscanf_s(recv_buffer, "%d %d %d %d", \
		&feedback[0], &feedback[1], &feedback[2], &feedback[3]);

	fprintf(stderr, "received: %i bytes\n"\
		"wrote: %i bytes\ndetected: %i errors\ncorrected: %i errors\n"\
		, feedback[0], feedback[1], feedback[2], feedback[3]);
	return chunk_recvd;
}

int main(int argc, char *argv[])
{
	if (check_arg_validity(argc, argv) == -1)
		return -1;
	char *recv_ip = argv[1];
	int port = atoi(argv[2]);
	char* file_name = argv[3];
	int sent = 0;

	/* initialization of socket and address */
	init_WinSock();
	SOCKET sender_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	struct sockaddr_in reciever_addr;
	struct sockaddr_in my_addr;
	/* convert INADDR_ANY to string represntaion for init_sockaddr_in() */
	int x = INADDR_ANY;
	char my_ip[20];
	inet_ntop(AF_INET, &x, my_ip, 20);

	/*initialize socket address and check for errors*/
	if ((init_sockaddr_in(&reciever_addr, recv_ip, port) == -1) || (init_sockaddr_in(&my_addr, my_ip, MY_PORT) == -1))
	{
		/* attempt closing the socket */
		if (closesocket(sender_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* bind the socket to IP and PORT */
	if (bind(sender_sock, (SOCKADDR*)&my_addr, sizeof(my_addr)) == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		if (closesocket(sender_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* send data from file to reciever. check for errors sending */
	sent = send_file(sender_sock, file_name, reciever_addr);
	if (sent == -1)
	{
		if (closesocket(sender_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	printf("sent %i bytes\n", sent);
	/* recv feedback from reciever*/
	if (recv_feedback(sender_sock) == -1)
	{
		/* attempt closing the socket */
		if (closesocket(sender_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	if (closesocket(sender_sock) == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	WSACleanup();
	system("pause");
	return 0;
}