#include <stdio.h>
#include <errno.h>
#include <WinSock2.h>
#include <WS2tcpip.h>
#include <windows.h>
#pragma comment(lib, "Ws2_32.lib")

#define FILE_BLOCK_SIZE 7
#define SOCKET_BLOCK_SIZE 8
#define BLOCKS_PER_PACKET 8
#define MAX_ERR_LEN 50

static unsigned int write_leftover_bits = 0;
static unsigned char write_leftover_char = 0;
static int end_flag = 0;
/* function to run thread for user input to wait for 'end'
 when end is input from the user the end_flag is changed
and the thread terminates */
DWORD WINAPI kb_input(void *data)
{
	char kb_buffer[4] = { 0 };
	char c;
	while (1)
	{
		c = getchar();
		kb_buffer[0] = kb_buffer[1];
		kb_buffer[1] = kb_buffer[2];
		kb_buffer[2] = c;
		if ((!strcmp(kb_buffer, "end")) || (!strcmp(kb_buffer, "End")))
		{
			end_flag = 1;
			break;
		}
	}
	return 0;
}

void print_win_api_error(DWORD error_code)
{
	char *error_msg;
	FormatMessage(
		FORMAT_MESSAGE_ALLOCATE_BUFFER |
		FORMAT_MESSAGE_FROM_SYSTEM |
		FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL,
		error_code,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR)&error_msg,
		0, NULL);
	fprintf(stderr, "Error: %s\n", error_msg);
	LocalFree(error_msg);
}

void print_crt_error(int error_code)
{
	char error_msg[MAX_ERR_LEN] = { 0 };
	strerror_s(error_msg, MAX_ERR_LEN, error_code);
	fprintf(stderr, "Error: %s\n", error_msg);
}

/* check whether the received parameters are valid port and file name. return -1 upon error */
int check_arg_validity(int argc, char *argv[])
{
	unsigned int i;
	int port;
	/*check for enough arguements*/
	if (argc != 3)
	{
		fprintf(stderr, "Incorrect number of arguements: %i expected 2 \n"\
			"parameters: <port> <file name>\n", argc - 1);
		return -1;
	}
	/* check that inputted port is a number */
	for (i = 0; i < strlen(argv[1]); i++)
	{
		if (!isdigit(argv[1][i]))
		{
			fprintf(stderr, "Error: port must be a number!\n");
			return -1;
		}
	}
	port = atoi(argv[1]);
	if ((port < 0) || (port > 65535))
	{
		fprintf(stderr, "Error: port number must be 0-65535!\n");
		return -1;
	}
	/* file path is checked when we try opening the file, no need to check here */
	return 0;
}

int init_WinSock()
{
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != NO_ERROR)
	{
		fputs("Error at WSAStartup()\n", stderr);
		return 1;
	}
	return 0;
}

/* rearrange the 64 encoded bits in src into the first 48 bits in dst. put the leftover in global variable
 return 0 upon success and -1 upon error */
int write_rearrange(unsigned char src_buffer[SOCKET_BLOCK_SIZE - 1], unsigned char dst_buffer[SOCKET_BLOCK_SIZE])
{
	int i;
	if ((!src_buffer) || (!dst_buffer))
	{
		fprintf(stderr, "Error: Couldn't rearrange NULL buffers");
		return -1;
	}
	for (i = 0; i < SOCKET_BLOCK_SIZE - 2; i++)
	{
		dst_buffer[i] = (src_buffer[i] << (8 - write_leftover_bits))\
			| (src_buffer[i + 1] >> (write_leftover_bits));
	}
	return 0;
}

/* check two dimensional parity and correct if possible, update feedback accordingly.
return 0 upon success and -1 upon error */
int detect_n_correct(unsigned char buffer[SOCKET_BLOCK_SIZE], int feedback[4])
{
	if (!buffer)
	{
		fprintf(stderr, "Error: couldn't add parity, buffer is NULL.\n");
		return -1;
	}
	unsigned int i, j;
	int count;
	int error_row_count = 0;
	int error_column_count = 0;
	int error_row = -1;
	int error_column = -1;
	for (i = 0; i < SOCKET_BLOCK_SIZE; i++)
	{
		count = 0;
		/* count the number of 1s in the i'th row to see if it has odd/even parity */
		for (j = 0; j < 8; j++)
		{
			if (buffer[i] & (1 << j))
				count++;
		}
		/* if parity is odd than error is detected in this i'th row */
		if (count % 2)
		{
			error_row_count++;
			error_row = i;
		}

		count = 0;
		/* count the number of 1s in the i'th column to see if it has odd/even parity */
		for (j = 0; j < 8; j++)
		{
			if (buffer[j] & (1 << (7 - i)))
				count++;
		}
		/* if parity is odd than error is detected in this i'th column  */
		if (count % 2)
		{
			error_column_count++;
			error_column = i;
		}
	}
	/* if there's error in exactly one row and one collums than it can be corrected */
	if ((error_row_count == 1) && (error_column_count == 1))
	{
		buffer[error_row] = buffer[error_row] ^ (1 << (7 - error_column));
		feedback[3] += 1;
	}
	if ((error_column_count > 0) || (error_row_count > 0))
		feedback[2] += 1;
	return 0;
}

/* write 49 bits to file, and store leftover in char and keep track of position with another variable
return the actual number of bits written to the file and -1 upon error*/
int decode_n_write(FILE *fp, unsigned char buffer[SOCKET_BLOCK_SIZE], int feedback[4])
{
	int bits_write = 0;
	int i;
	unsigned char write_buff[SOCKET_BLOCK_SIZE - 1] = { 0 };
	/* detect and correct (if possible) errors in data received */
	detect_n_correct(buffer, feedback);

	write_buff[0] = write_leftover_char;
	/* rearrage and extract the data from the encoded message into write_buff */
	for (i = 1; i < SOCKET_BLOCK_SIZE - 1; i++)
	{
		/* the data bits are ordered in a 7x7 bit matrix. the last (7th) data row in the matrix is originally
		last bit of each byte in the former 6 rows (recall the convention mentioned in the read_n_encode function) */
		write_buff[i] = buffer[i - 1] & ~1;
		/* shift and mask to get the i'th bit from the left as the LSB */
		write_buff[i] = write_buff[i] | ((buffer[SOCKET_BLOCK_SIZE - 2] >> (8 - i)) & 1);
	}

	/* set the next leftover write character */
	write_leftover_char = write_buff[SOCKET_BLOCK_SIZE - 2];
	/* the 49th bit is in the buffer in the 7th row and 7th collumn. we add it to the leftover chars as the LSB */
	write_leftover_char = (write_leftover_char << 1) | ((buffer[SOCKET_BLOCK_SIZE - 2] >> 1) & 1);

	/* rearrange to get bits into buffer according to leftover count */
	write_rearrange(write_buff, buffer);
	/* the leftover bits are incremented since we received 49 bits and write to the file 48 */
	write_leftover_bits++;

	/* write the 48 bits to the file */
	fwrite(buffer, FILE_BLOCK_SIZE - 1, 1, fp);
	bits_write += (FILE_BLOCK_SIZE - 1) * 8;
	feedback[1] += FILE_BLOCK_SIZE - 1;

	/* if there are already 8 bits in leftover we can write them too */
	if (write_leftover_bits == 8)
	{
		fwrite(&write_leftover_char, 1, 1, fp);
		bits_write += 8;
		write_leftover_bits = 0;
		write_leftover_char = 0;
		feedback[1] ++;
	}
	if (ferror(fp))
	{
		print_crt_error(errno);
		return -1;
	}
	return bits_write;
}

/* initialize socket address structure. return 0 when successful and -1 upon error */
int init_sockaddr_in(struct sockaddr_in *addr, char *ip, int port)
{
	if (!addr)
	{
		fprintf(stderr, "Error: coudln't initialzie. NULL address pointer.\n");
		return -1;
	}
	addr->sin_family = AF_INET; /* internetwork protocol family*/
	InetPton(AF_INET, ip, &(addr->sin_addr)); /* loopback IP */
	addr->sin_port = htons(port); /* PORT */
	return 0;
}

/* return number of bytes recvd or -1 upon error */
int recv_data(SOCKET s, char *file_name, struct sockaddr_in *source_addr, int feedback[4])
{
	int i;
	int chunk_recvd = 0;
	char recv_buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET] = { 0 };
	FILE *fp;
	socklen_t addr_len = sizeof(*source_addr);
	fd_set read_fds;
	TIMEVAL select_timeout;
	select_timeout.tv_sec = 0;
	select_timeout.tv_usec = 0;

	/* open file for writing and handle errors if any */
	fopen_s(&fp, file_name, "wb");
	if (!fp)
	{
		print_crt_error(errno);
		return -1;
	}

	printf("Type End when done\n");
	/* create and start thread for user kb input, handle errors if any */
	if (!CreateThread(NULL, 0, kb_input, NULL, 0, NULL))
	{
		print_win_api_error(GetLastError());
		return -1;
	}
	while (!end_flag)
	{
		/* zero the read_fds file descriptors list, add the receiver socket
		and than set the status with FD_SET and poll the result with FD_ISSET */
		FD_ZERO(&read_fds);
		FD_SET(s, &read_fds);
		if (select(0, &read_fds, NULL, NULL, &select_timeout) == -1)
		{
			print_crt_error(errno);
			return -1;
		}
		if (FD_ISSET(s, &read_fds))
		{
			/* recv data from socket and check for recv errors */
			chunk_recvd = recvfrom(s, recv_buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, 0, \
				(SOCKADDR*)source_addr, &addr_len);
			if (chunk_recvd == SOCKET_ERROR)
			{
				print_win_api_error(WSAGetLastError());
				return -1;
			}
			/* decode data and write to file. check for file write errors */
			for (i = 0; i < BLOCKS_PER_PACKET; i++)
				if (decode_n_write(fp, recv_buffer + SOCKET_BLOCK_SIZE * i, feedback) == -1)
					return -1;
			feedback[0] += chunk_recvd;
		}
	}
	fclose(fp);
	fprintf(stderr, "received: %i bytes\n"\
		"wrote: %i bytes\ndetected: %i errors\ncorrected: %i errors\n"\
		, feedback[0], feedback[1], feedback[2], feedback[3]);
	return feedback[0];
}

/* send feedback to sender. return number of bytes sent or -1 upon error*/
int send_feedback(SOCKET s, struct sockaddr_in addr, int feedback[4])
{
	int to_send = 0;
	int sent = 0;
	char buffer[SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET];
	to_send = 1 + sprintf_s(buffer, SOCKET_BLOCK_SIZE*BLOCKS_PER_PACKET, "%i %i %i %i", \
		feedback[0], feedback[1], feedback[2], feedback[3]);
	sent += sendto(s, buffer, to_send, 0, (SOCKADDR*)&addr, sizeof(addr));
	if (sent == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		return -1;
	}
	return sent;
}

int main(int argc, char *argv[])
{
	if (check_arg_validity(argc, argv) == -1)
		return -1;
	long int port = atoi(argv[1]);
	char* file_name = argv[2];
	/* feedback array, [0] is bytes recvd [1] is bytes written 
	[2] is errors detected [3] is arror corrected */
	int feedback[4] = { 0 };
	int status = 0;

	/* initialization of socket and address */
	init_WinSock();
	SOCKET receiver_sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	struct sockaddr_in my_addr;
	struct sockaddr_in source_addr;
	/* convert INADDR_ANY to string represntaion for init_sockaddr_in() */
	int x = INADDR_ANY;
	char my_ip[20];
	inet_ntop(AF_INET, &x, my_ip, 20);

	/*initialize socket address and check for errors*/
	if (init_sockaddr_in(&my_addr, my_ip, port) == -1)
	{
		/* attempt closing the socket */
		if (closesocket(receiver_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* bind the socket to IP and PORT */
	if (bind(receiver_sock, (SOCKADDR*)&my_addr, sizeof(my_addr)) == SOCKET_ERROR)
	{
		print_crt_error(errno);
		if (closesocket(receiver_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* recv data from the sender */
	if (recv_data(receiver_sock, file_name, &source_addr, feedback) == -1)
	{
		if (closesocket(receiver_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	/* send feedback to sender */
	if (send_feedback(receiver_sock, source_addr, feedback) == -1)
	{
		if (closesocket(receiver_sock) == SOCKET_ERROR)
			print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}
	if (closesocket(receiver_sock) == SOCKET_ERROR)
	{
		print_win_api_error(WSAGetLastError());
		WSACleanup();
		return 1;
	}	WSACleanup();
	system("pause");
	return 0;
}